/*
 * Copyright (c) 2020-2021 Advanced Robotics at the University of Washington <robomstr@uw.edu>
 *
 * This file is part of aruw-mcb.
 *
 * aruw-mcb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * aruw-mcb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with aruw-mcb.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DRIVERS_HPP_
#define DRIVERS_HPP_

#include "tap/drivers.hpp"
#include "tap/communication/sensors/imu/imu_terminal_serial_handler.hpp"


namespace template-project
{
class Drivers : public tap::Drivers
{
    friend class DriversSingleton;

    Drivers()
        : tap::Drivers(),
        //   controlOperatorInterface(this),
        //   visionCoprocessor(this),
        //   oledDisplay(this),
        //   turretMCBCanCommBus1(this, tap::can::CanBus::CAN_BUS1),
        //   turretMCBCanCommBus2(this, tap::can::CanBus::CAN_BUS2),
        //   mpu6500TerminalSerialHandler(this, &this->mpu6500)
    {
    }

public:
    // control::ControlOperatorInterface controlOperatorInterface;
    // serial::VisionCoprocessor visionCoprocessor;
    // display::OledDisplay oledDisplay;
    // can::TurretMCBCanComm turretMCBCanCommBus1;
    // can::TurretMCBCanComm turretMCBCanCommBus2;
    tap::communication::sensors::imu::ImuTerminalSerialHandler mpu6500TerminalSerialHandler;
};  // class aruwsrc::Drivers
}

#endif  // DRIVERS_HPP_