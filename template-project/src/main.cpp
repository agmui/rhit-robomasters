#include "tap/algorithms/smooth_pid.hpp"
#include "tap/board/board.hpp"

#include "drivers_singleton.hpp"
#include "tap/communication/serial/dji_serial.hpp"
#include "template-project/drivers_singleton.hpp"
#include "template-project/control/robot_control.hpp"

// static constexpr tap::motor::MotorId MOTOR_ID = tap::motor::MOTOR1;
// static constexpr tap::can::CanBus CAN_BUS = tap::can::CanBus::CAN_BUS1;
// static constexpr int DESIRED_RPM = 3000;

// tap::arch::PeriodicMilliTimer sendMotorTimeout(2);
// tap::algorithms::SmoothPid pidController(20, 0, 0, 0, 8000, 1, 0, 1, 0);
// tap::motor::DjiMotor motor(::DoNotUse_getDrivers(), MOTOR_ID, CAN_BUS, false, "cool motor");

int main()
{
    /*
     * NOTE: We are using DoNotUse_getDrivers here because in the main
     *      robot loop we must access the singleton drivers to update
     *      IO states and run the scheduler.
     */
    ::Drivers *drivers = ::DoNotUse_getDrivers();

    Board::initialize();
    // drivers->remote.initialize();
    drivers->leds.init();
    // drivers->can.initialize();
    // motor.initialize();
    // drivers->TestSerial.initializeCV();//maybe dont use

    tap::gpio::Digital pin;
    pin.init();
    // drivers->digital.init()
    // pin.configureInputPullMode(pin.A, tap::gpio::Digital::InputPullMode::PullDown);
    bool aSet = true;

    // : DJISerial(drivers, VISION_COPROCESSOR_RX_UART_PORT);
    const tap::communication::serial::DJISerial::ReceivedSerialMessage message;
    

    while (1)
    {

        // drivers->visionCoprocessor.updateSerial();
        tap::communication::serial::DJISerial::updateSerial();


        /*gpio
        int reading = pin.read(tap::gpio::Digital::InputPin::B);
        bool reading = drivers->digital.read(tap::gpio::Digital::A);
        drivers->leds.set(tap::gpio::Leds::G, reading);
        */

        //ts
        // if(reading){
        //     modm::delay_ms(1000/ 2);
        //     drivers->leds.set(tap::gpio::Leds::A, !aSet);
        //     drivers->leds.set(tap::gpio::Leds::B, !aSet);
        //     aSet = !aSet;
        // }
    }
    return 0;
}
