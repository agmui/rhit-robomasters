from cv2 import INTER_MAX
import pyrealsense2.pyrealsense2 as rs
import numpy as np
import cv2
import filters

# lower_blu = [122, 84, 30]
# upper_blu = [125, 131, 219]
# lower_white = [0, 0, 234]
# upper_white = [132, 18, 242]
lower_blu = [0, 0, 0]
upper_blu = [180, 255, 255]
lower_white = [0, 0, 200]
upper_white = [180, 255, 255]
exposure_val = 10


def hue_lower(H):
    lower_white[0] = H


def sat_lower(S):
    lower_white[1] = S


def val_lower(V):
    lower_white[2] = V


def hue_upper(H):
    upper_white[0] = H


def sat_upper(S):
    upper_white[1] = S


def val_upper(V):
    upper_white[2] = V


def change_exposure(val):
    global exposure_val
    exposure_val = val


def HSV(image, name):
    result = image.copy()

    image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)  # converts BGR to HSV

    # ==just blu==
    lower_arr = np.array(lower_blu)
    upper_arr = np.array(upper_blu)

    full_mask = cv2.inRange(image, lower_arr, upper_arr)

    result_blu = cv2.bitwise_and(result, result, mask=full_mask)

    # ==just white==
    lower_arr = np.array(lower_white)
    upper_arr = np.array(upper_white)

    full_mask = cv2.inRange(image, lower_arr, upper_arr)

    result_white = cv2.bitwise_and(result, result, mask=full_mask)

    return result_blu, result_white


name: str = "RealSense"
cv2.namedWindow(name)
cv2.createTrackbar("HL", name, 0, 180, hue_lower)
cv2.createTrackbar("SL", name, 0, 255, sat_lower)
cv2.createTrackbar("VL", name, 0, 255, val_lower)
cv2.setTrackbarPos("VL", name, lower_white[2])
cv2.createTrackbar("HU", name, 0, 180, hue_upper)
cv2.setTrackbarPos("HU", name, 180)
cv2.createTrackbar("SU", name, 0, 255, sat_upper)
cv2.setTrackbarPos("SU", name, 255)
cv2.createTrackbar("VU", name, 0, 255, val_upper)
cv2.setTrackbarPos("VU", name, 255)

# Configure depth and color streams
pipeline = rs.pipeline()
config = rs.config()

# Get device product line for setting a supporting resolution
pipeline_wrapper = rs.pipeline_wrapper(pipeline)
pipeline_profile = config.resolve(pipeline_wrapper)
device = pipeline_profile.get_device()
# ==change exposure==
s = device.query_sensors()[1]
cv2.createTrackbar("exposure", name, 0, 255, change_exposure)
cv2.setTrackbarPos("exposure", name, exposure_val)
device_product_line = str(device.get_info(rs.camera_info.product_line))
# ====

config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)

if device_product_line == 'L500':
    config.enable_stream(rs.stream.color, 960, 540, rs.format.bgr8, 30)
else:
    config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)

# Start streaming
pipeline.start(config)

try:
    while True:

        s.set_option(rs.option.exposure, exposure_val)
        # Wait for a coherent pair of frames: depth and color
        frames = pipeline.wait_for_frames()
        color_frame = frames.get_color_frame()

        # Convert images to numpy arrays
        color_image = np.asanyarray(color_frame.get_data())

        # ==opencv==
        image = color_image
        blu, img_to_op = HSV(image, name)

        keypoints = filters.blob_detection(img_to_op)

        img_to_op = filters.ROI(img_to_op, keypoints)

        # ===drawing boxes/perspective transform===
        gray = cv2.cvtColor(img_to_op, cv2.COLOR_BGR2GRAY)
        thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_OTSU)[1]

        cnts = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts = cnts[0] if len(cnts) == 2 else cnts[1]

        keypoints = []
        index = 0  # make it so the comparisons are in an arithmetic series

        # init dic
        dic = {}
        for i in range(len(cnts)):
            dic[i + 1] = False

        for c in cnts:
            min: float = 10000
            min_cord = None
            index += 1

            # drawing box
            x, y, w, h = cv2.boundingRect(c)
            # TODO plz don't draw on img
            cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)
            # if bar is already part of a group just skip
            if dic[index]:
                continue
            index2 = index
            bar2_index = 0
            # start of the distance comparisons loop
            for i in cnts[index:]:
                index2 += 1
                x2, y2, w2, h2 = cv2.boundingRect(i)
                if x == x2:  # should not be on the same x-axis
                    continue
                # finding min dist between bars
                dist = np.hypot(x, x2)
                if min > dist:
                    min = dist
                    min_cord = [x2, y2, w2, h2]
                    bar2_index = index2
            # draw rect for pannal
            if min_cord == None:
                continue
            dic[index] = True
            dic[bar2_index] = True
            cv2.rectangle(image, (x, y - h),
                          (min_cord[0] + min_cord[2], min_cord[1] + min_cord[3] * 2),
                          (255, 0, 0), 2)
            print("plate cord:",
                  (min_cord[0] + min_cord[2] + x) / 2,
                  (min_cord[1] + min_cord[3] * 2 + y - h) / 2)

        # Show images
        cv2.imshow('RealSense', color_image)
        k = cv2.waitKey(1)
        if k == ord("q"):
            cv2.destroyAllWindows()
            break

finally:

    # Stop streaming
    pipeline.stop()
