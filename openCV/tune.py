import sys
import cv2
import numpy as np
import filters

blue_bool: int = 0
canny_bool: int = 0
contoure_bool: int = 0
cam_view_bool: int = 0

name: str = "final output"
cv2.namedWindow(name)
cv2.moveWindow(name, 1935, 10)
cv2.displayOverlay(name, name)  # top text

filters.make_slid(name)
cv2.createButton('blue', filters.on_radio, "blue", 1, 0)
cv2.createButton('canny ED', filters.on_radio, "canny", 1, 0)
cv2.createButton('contours', filters.on_radio, "contours", 1, 0)
cv2.createButton('cam view', filters.on_radio, "cam view", 1, 0)


def main(image):
    img_to_op, black = filters.RGB(image)

    keypoints = filters.blob_detection(img_to_op)

    img_to_op = filters.ROI(img_to_op, keypoints)

    # ===drawing boxes/perspective transform===
    gray = cv2.cvtColor(img_to_op, cv2.COLOR_BGR2GRAY)
    thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_OTSU)[1]

    cnts = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = cnts[0] if len(cnts) == 2 else cnts[1]

    index = 0  # make it so the comparisons are in an arithmetic series
    for c in cnts:
        index += 1
        # drawing box
        x, y, w, h = cv2.boundingRect(c)
        # TODO plz don't draw on img
        cv2.rectangle(image, (x, y), (x + w, y + h), (0, 0, 255), 2)
        """for i in cnts[index:]:
            # if c is i:  # just double check
            #     continue
            # perspective transform
            x2, y2, w2, h2 = cv2.boundingRect(i)
            if x == x2:  # should not be on the same x-axis
                continue

            perspective_transform(image,
                                  [[x - h * .1, y - h * .6], [x2 + h2 * .1 + w2, y2 - h * .6],
                                   [x - h * .1, y + h * 1.5], [x2 + h2 * .1 + w2, y2 + h2 * 1.5]]
                                  )
                                  """

    # sobel_ED(image)
    if canny_bool:
        filters.canny_ED(image)
    if contoure_bool:
        filters.contours(image)
    if cam_view_bool:
        cv2.imshow("final cut down", img_to_op)
    # try this later
    # images = np.hstack((color_image, depth_colormap))
    cv2.imshow(name, image)


"""cap = cv2.VideoCapture(0)
# Check if camera opened successfully
if not cap.isOpened():
    print("Error opening video  file")
# Read until video is completed
while cap.isOpened():
    ret, frame = cap.read()
    if not ret:
        break
    main(frame)
    if cv2.waitKey(25) & 0xFF == ord('q'):
        break
cap.release()

cv2.destroyAllWindows()
"""

image = cv2.imread(cv2.samples.findFile("pic/IMG_0434.JPG"))
# image = cv2.imread(cv2.samples.findFile("pic/ogCalibration.png"))
image = cv2.resize(image, (image.shape[1]//3, image.shape[0]//3))  # Resize image

# cv2.resizeWindow("HSV", 300, 700)
# cv2.resizeWindow(name, 300, 700)

main(image)

while (True):
    k = cv2.waitKey()
    if k == ord("q"):
        break

cv2.destroyAllWindows()
