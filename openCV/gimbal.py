import math
import os.path
import numpy as np
import cv2
from definitions import ROOT_DIR
import pyrealsense2

# import rospy
# from sensor_msgs.msg import Image as msg_Image
# from sensor_msgs.msg import CameraInfo
# from cv_bridge import CvBridge, CvBridgeError
import sys
import os
import pyrealsense2 as rs2

import pyrealsense2 as rs
import numpy as np



def pixel_to_3d(pixel, color_frame, depth_frame, pipe_profile):
    depth_intrin = depth_frame.profile.as_video_stream_profile().intrinsics
    color_intrin = color_frame.profile.as_video_stream_profile().intrinsics
    depth_to_color_extrin = depth_frame.profile.get_extrinsics_to(color_frame.profile)
    color_to_depth_extrin = color_frame.profile.get_extrinsics_to(depth_frame.profile)
    # print("\n Depth intrinsics: " + str(depth_intrin))
    # print("\n Color intrinsics: " + str(color_intrin))
    # print("\n Depth to color extrinsics: " + str(depth_to_color_extrin))

    depth_sensor = pipe_profile.get_device().first_depth_sensor()
    depth_scale = depth_sensor.get_depth_scale()
    # print("\n\t depth_scale: " + str(depth_scale))
    depth_image = np.asanyarray(depth_frame.get_data())
    depth_pixel = [pixel[0], pixel[1]]  # Random pixel
    depth_value = depth_image[200][200] * depth_scale
    # print("\n\t depth_pixel@" + str(depth_pixel) + " value: " + str(depth_value) + " meter")

    depth_point = rs.rs2_deproject_pixel_to_point(depth_intrin, depth_pixel, depth_value)

    return depth_point

def convert(panelCords, color_f, depth_frame, pipe_profile):
    for point in panelCords:
        irl_point = pixel_to_3d(point, color_f, depth_frame, pipe_profile)
        print(str(irl_point))

        theta = math.atan2(irl_point[1], irl_point[0])
        phi = math.acos(irl_point[2])
        print(f"theta = {theta}, phi = {phi}")
        return theta, phi


# we might need this later
def imageDepthInfoCallback(cameraInfo):
    intrinsics = rs2.intrinsics()
    intrinsics.width = cameraInfo.width
    intrinsics.height = cameraInfo.height
    intrinsics.ppx = cameraInfo.K[2]
    intrinsics.ppy = cameraInfo.K[5]
    intrinsics.fx = cameraInfo.K[0]
    intrinsics.fy = cameraInfo.K[4]
    if cameraInfo.distortion_model == 'plumb_bob':
        intrinsics.model = rs2.distortion.brown_conrady
    elif cameraInfo.distortion_model == 'equidistant':
        intrinsics.model = rs2.distortion.kannala_brandt4
    intrinsics.coeffs = [i for i in cameraInfo.D]


def demo():
    # Configure depth and color streams
    config = rs.config()
    config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
    config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)
    pipeline = rs.pipeline()

    # Get device product line for setting a supporting resolution
    pipeline_wrapper = rs.pipeline_wrapper(pipeline)
    pipeline_profile = config.resolve(pipeline_wrapper)
    device = pipeline_profile.get_device()
    device_product_line = str(device.get_info(rs.camera_info.product_line))

    found_rgb = False
    for s in device.sensors:
        if s.get_info(rs.camera_info.name) == 'RGB Camera':
            found_rgb = True
            break
    if not found_rgb:
        print("The demo requires Depth camera with Color sensor")
        exit(0)

    config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)

    if device_product_line == 'L500':
        config.enable_stream(rs.stream.color, 960, 540, rs.format.bgr8, 30)
    else:
        config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)

    pipe_profile = pipeline.start(config)
    try:
        while True:

            # Wait for a coherent pair of frames: depth and color
            frames = pipeline.wait_for_frames()
            depth_frame = frames.get_depth_frame()
            color_frame = frames.get_color_frame()
            if not depth_frame or not color_frame:
                continue

            # Convert images to numpy arrays
            depth_image = np.asanyarray(depth_frame.get_data())
            color_image = np.asanyarray(color_frame.get_data())

            # Apply colormap on depth image (image must be converted to 8-bit per pixel first)
            depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha=0.03), cv2.COLORMAP_JET)

            point = [300, 150]
            for i in range(point[1] - 5, point[1] + 5):
                for j in range(point[0] - 5, point[0] + 5):
                    color_image[i, j] = (0, 255, 0)
                    depth_colormap[i, j] = (0, 255, 0)

            point = [point[0], 480 - point[1]]
            irl_point = pixel_to_3d(point, color_frame, depth_frame, pipe_profile)
            print(str(irl_point))

            theta = math.atan2(irl_point[1], irl_point[0])
            phi = math.acos(irl_point[2])
            print(f"theta = {theta}, phi = {phi}")

            depth_colormap_dim = depth_colormap.shape
            color_colormap_dim = color_image.shape

            # If depth and color resolutions are different, resize color image to match depth image for display
            if depth_colormap_dim != color_colormap_dim:
                resized_color_image = cv2.resize(color_image, dsize=(depth_colormap_dim[1], depth_colormap_dim[0]),
                                                 interpolation=cv2.INTER_AREA)
                images = np.hstack((resized_color_image, depth_colormap))
            else:
                images = np.hstack((color_image, depth_colormap))

            # Show images
            # cv2.namedWindow('RealSense', cv2.WINDOW_AUTOSIZE)
            cv2.imshow('RealSense', images)
            k = cv2.waitKey(1)
            if k == ord("q"):
                cv2.destroyAllWindows()
                break

    finally:

        # Stop streaming
        print("stop")
        pipeline.stop()

if __name__ == "__main__":
    demo()
