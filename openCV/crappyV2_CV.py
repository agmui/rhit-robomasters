import math
import os.path
import numpy as np
import cv2
import filters
import sys
# import imutils
from definitions import ROOT_DIR


def findRect(box, box2, img):  # TODO optimise
    s = box.sum(axis=1)
    TL = box[np.argmin(s)]
    diff = np.diff(box, axis=1)
    BL = box[np.argmax(diff)]

    s = box2.sum(axis=1)
    TR = box2[np.argmax(s)]
    diff = np.diff(box2, axis=1)
    BR = box2[np.argmin(diff)]

    # cv2.circle(img, TL, 3, (255, 0, 0), 3)  # blue
    # cv2.circle(img, BR, 3, (255, 255, 255), 3)  # white
    # cv2.circle(img, BL, 3, (0, 255, 0), 3)  # green
    # cv2.circle(img, TR, 3, (0, 0, 255), 3)  # red

    # create linear equation
    points = [TL, BL]
    x_coords, y_coords = zip(*points)
    A = np.vstack([x_coords, np.ones(len(x_coords))]).T
    m, c = np.linalg.lstsq(A, y_coords, rcond=None)[0]
    # top offset
    f = lambda x1: int(m * x1 + c)
    x = (TL[0] - BL[0]) * .85
    if x == 0:
        dist = TL[1] - BL[1]
        TL[1] += dist * 0.9
        BL[1] -= dist * 0.9
    else:
        TL = (int(TL[0] + x), f(TL[0] + x))
        BL = (int(BL[0] - x), f(BL[0] - x))

    points = [TR, BR]
    x_coords, y_coords = zip(*points)
    A = np.vstack([x_coords, np.ones(len(x_coords))]).T
    m2, c2 = np.linalg.lstsq(A, y_coords, rcond=None)[0]
    f = lambda x1: int(m2 * x1 + c2)
    x = (TR[0] - BR[0]) * .85
    if x == 0:
        dist = TR[1] - BR[1]
        TR[1] += dist * .9
        BR[1] -= dist * .9
    else:
        TR = (int(TR[0] + x), f(TR[0] + x))
        BR = (int(BR[0] - x), f(BR[0] - x))

    # cv2.circle(img, TL, 3, (255, 0, 0), 3)  # blue
    # cv2.circle(img, BR, 3, (255, 255, 255), 3)  # white
    # cv2.circle(img, BL, 3, (0, 255, 0), 3)  # green
    # cv2.circle(img, TR, 3, (0, 0, 255), 3)  # red
    return TL, BR, BL, TR


def objectDetection(image, enemyColor, showExtra=False) -> list:
    """
    this code assumes that all the panels will not be aligned next to each other.
    The major flaws are that if two bars that are from different panels
    are too close to each other than the code will not know which is which.

    Also, the code has a set (called dic) that checks if a bar has already been paired
    this only works under the assumption above.
    """
    imgToDrawOn = image.copy()

    RGBimg = filters.RGB(image, enemyColor)

    keypoints = filters.blob_detection(RGBimg, True)

    RGBimg = filters.ROI(RGBimg, keypoints)

    # ===drawing boxes/perspective transform===
    cnts = filters.contours(RGBimg)

    # index = -1  # make it so the comparisons are in an arithmetic series
    # init dic
    dic = {}  # this dic tradesman if the bar has been used yet or not
    for pixel in range(len(cnts)):
        dic[pixel] = False

    panelCords = []
    # for c in cnts:
    for index in range(len(cnts)):  # FIXME
        minDist: float = 10000  # TODO make it already compair the first pair
        minRect, minTL2 = None, None
        # index += 1

        # drawing box
        rect = cv2.minAreaRect(cnts[index])
        box = cv2.boxPoints(rect)
        intBox = np.int0(box)
        # find top left cord
        s = intBox.sum(axis=1)
        topLeft = intBox[np.argmin(s)]

        # cv2.drawContours(imgToDrawOn, [intBox], 0, (0, 255, 0), 2)
        cv2.putText(imgToDrawOn, str(index), topLeft, cv2.FONT_HERSHEY_PLAIN, 1, (0, 255, 0), 1)
        # if bar is already measured skip
        if dic[index]:
            # print("already has closest")
            continue
        _index2 = 0  # just a placeholder name for index2
        # start of the distance comparisons loop
        for index2 in range(len(cnts[(index + 1):])):
            index2 += index + 1  # dont del
            # print(index, index2)  # ts
            rect2 = cv2.minAreaRect(cnts[index2])
            box2 = cv2.boxPoints(rect2)
            intBox2 = np.int0(box2)

            s = intBox2.sum(axis=1)
            topLeft2 = intBox2[np.argmin(s)]
            if dic[index2]:  # if cnts already has a pair
                # print("already has pair")
                continue
            # elif topLeft[1] == topLeft2[1]:  # TODO make in range
            #     continue

            # finding min dist between bars
            dist = math.dist(topLeft, topLeft2)
            if dist < minDist:
                minDist = dist
                minRect = intBox2  # TODO
                minTL2 = topLeft2
                _index2 = index2
                # print("min dist", index, index2)

        if minRect is None:  # should never happen
            continue
        # ensures the bars stay in the middle
        if minTL2[0] > topLeft[0]:  # if box is on left and box2 is on right
            plateRect = findRect(intBox, minRect, imgToDrawOn)
        else:  # if box is on the right and box2 is on left
            plateRect = findRect(minRect, intBox, imgToDrawOn)
        # cv2.line(imgToDrawOn, plateRect[0], plateRect[2], (0, 0, 255), 1)
        # cv2.line(imgToDrawOn, plateRect[1], plateRect[3], (0, 0, 255), 1)

        result = filters.perspective_transform(image, plateRect, enemyColor)  # TODO ======================

        # TODO filter out all other noise and only have panel
        # blue = filters.HSV(result, 'blue')
        # black = filters.HSV(result,'black')
        # result = blue + black
        # cv2.imshow(str(topLeft[0]), result)

        # print("=== panel:", index, _index2, "====")
        th = filters.adaptiveGaussianThresh(result, False)
        isPanel = filters.convolutionMatching(th, enemyColor, False, result, verbose=False)
        # print(isPanel)
        # the return determines if the bar can be used again or not
        dic[index] = isPanel
        dic[_index2] = isPanel

        pts = np.array([plateRect[0], plateRect[1], plateRect[3], plateRect[2]], np.int32)

        if isPanel:
            cv2.polylines(imgToDrawOn, [pts], True, (0, 255, 0), 2)
            x = [p[0] for p in pts]
            y = [p[1] for p in pts]
            centroid = (sum(x) / len(pts), sum(y) / len(pts))
            print("plate cord", centroid)
            panelCords.append(centroid)
        # else:
            # cv2.polylines(imgToDrawOn, [pts], True, (0, 0, 255), 1)

    if showExtra:
        # Show images
        cv2.imshow('RealSense', imgToDrawOn)
    return panelCords, imgToDrawOn


if __name__ == '__main__':
    # image = cv2.imread(os.path.join(ROOT_DIR, "pic/multiBluPanel.jpg"))  # multi panel
    # image = cv2.imread(os.path.join(ROOT_DIR, "pic/edited2panelBlu.jpg"))
    image = cv2.imread(os.path.join(ROOT_DIR, "pic/2panelBlu.jpg"))
    # image = cv2.imread(os.path.join(ROOT_DIR, "pic/2panelRed.jpg"))
    # image = cv2.imread(cv2.samples.findFile("pic/IMG_0432.JPG"))
    # image = cv2.imread(cv2.samples.findFile("pic/IMG_0415.JPG"))
    # image = cv2.imread(cv2.samples.findFile("pic/IMG_0416.JPG"))
    # image = cv2.imread(cv2.samples.findFile("pic/IMG_0427.JPG"))
    image = cv2.imread(os.path.join(ROOT_DIR,"pic/IMG_0428.JPG"))
    # image = cv2.imread(cv2.samples.findFile("pic/IMG_0429.JPG"))
    image = cv2.resize(image, (image.shape[1] // 4, image.shape[0] // 4))  # Resize image
    # image = cv2.imread(cv2.samples.findFile("pic/ogCalibration.png"))

    if image is None:
        sys.exit("Could not read the image.")

    panelCords = objectDetection(image, 'red', True)
    print(panelCords)

    while True:
        k = cv2.waitKey(1)
        if k == ord("q"):
            cv2.destroyAllWindows()
            break
