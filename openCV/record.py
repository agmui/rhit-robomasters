import pyrealsense2.pyrealsense2 as rs
import cv2
import numpy as np
from datetime import datetime

# ========init code for intel RealSense=======
# def initCam():
# Configure depth and color streams
pipeline = rs.pipeline()
config = rs.config()
today = datetime.now()
d1 = today.strftime("%d_%m_%Y %H:%M:%S")
config.enable_record_to_file('recordings/recording_'+d1+'.bag')

# Get device product line for setting a supporting resolution
pipeline_wrapper = rs.pipeline_wrapper(pipeline)
pipeline_profile = config.resolve(pipeline_wrapper)
device = pipeline_profile.get_device()
# ==change exposure==
s = device.query_sensors()[1]
device_product_line = str(device.get_info(rs.camera_info.product_line))

config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)

if device_product_line == 'L500':
    config.enable_stream(rs.stream.color, 960, 540, rs.format.bgr8, 30)
else:
    config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)

# Start streaming
pipeline.start(config)

try:
    while True:
        # Wait for a coherent pair of frames: depth and color
        frames = pipeline.wait_for_frames()
        color_frame = frames.get_color_frame()

        # Convert images to numpy arrays
        color_image = np.asanyarray(color_frame.get_data())

        # ==opencv display ===
        cv2.imshow("RealSense", color_image)

        k = cv2.waitKey(1)
        if k == ord("q"):
            cv2.destroyAllWindows()
            break


finally:
    # Stop streaming
    pipeline.stop()