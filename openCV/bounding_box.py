import sys
import cv2
import numpy as np
import filters

image = cv2.imread(cv2.samples.findFile("pic/IMG_0434.JPG"))
image = cv2.resize(image, (image.shape[1] // 3, image.shape[0] // 3))  # Resize image

if image is None:
    sys.exit("Could not read the image.")

name: str = "final output"
cv2.namedWindow(name)
cv2.moveWindow(name, 2500, 150)

bluImg, blackImg = filters.RGB(image, False)

keypoints = filters.blob_detection(bluImg, False)

bluImg = filters.ROI(bluImg, keypoints, False)

cnts = filters.contours(bluImg, False)

# ===drawing boxes/perspective transform===
keypoints = []
index = -1  # make it so the comparisons are in an arithmetic series
for c in cnts:
    index += 1
    # drawing box
    x, y, w, h = cv2.boundingRect(c)
    # TODO plz don't draw on img
    cv2.rectangle(image, (x, y), (x + w, y + h), (0, 0, 255), 2)
    # note not all rects will be drawn

    # temp: int = index + 1
    for i in cnts[(index + 1):]:
        # print(index, temp)
        # temp += 1

        # perspective transform
        x2, y2, w2, h2 = cv2.boundingRect(i)  # FIXME
        if x == x2:  # should not be on the same x-axis
            continue

        point = cv2.KeyPoint()
        point.pt = (
            (x + x2 + w2) / 2, (y + y2 + h2) / 2  # getting the middle of the 2 bars
        )
        point.size = h * 1.5
        keypoints.append(point)

        # plate_guess = filters.ROI(image, keypoints)
        # cv2.imshow("hi", plate_guess)
        # if index == 1 and temp == 4:
        filters.perspective_transform(image,
                                      [[x - h * .1, y - h * .6], [x2 + h2 * .1 + w2, y2 - h * .6],
                                       [x - h * .1, y + h * 1.5], [x2 + h2 * .1 + w2, y2 + h2 * 1.5]]
                                      )

# cv2.imshow("final cut down", img_to_op)
cv2.imshow(name, image)

while True:
    k = cv2.waitKey(0)
    if k == ord("q"):
        cv2.destroyAllWindows()
        break
