# loading library
import math
import cv2
import numpy as np
import os


def angled_motion_blur(img, angle, kernel_size=40):
    # Specify the kernel size.
    # The greater the size, the more the motion.
    # draw AA line
    kernel = None
    if angle == 90 or angle == -90:  # for vertical blur
        # Fill the middle row with ones.
        kernel = np.zeros((kernel_size, kernel_size))
        kernel[int((kernel_size - 1) / 2), :] = np.ones(kernel_size)
    else:
        kernel = np.zeros((kernel_size, kernel_size), np.uint8)
        # angle = -angle  # cuz images cords are flipped with 0,0 at top left

        # we first find the slope of the line
        slope = math.tan(math.radians(angle))
        # then we just need calculate y cord of the line to do that we take the slope and add or sub 1 to "translate"
        # it to the first quadrant by adding 1 to everything. Then scale it up by half the kernel size
        # NOTE: the + or - for slope is flipped cuz imgs (0,0) starts on the top left
        p1 = (0, int((kernel_size - 1) / 2 * (1 + slope)))
        p2 = (kernel_size - 1, int((kernel_size - 1) / 2 * (1 - slope)))

        cv2.line(kernel, p1, p2, 255, 1, cv2.LINE_AA)
        kernel = kernel.astype(float) / 255
        # cv2.namedWindow('kernel', cv2.WINDOW_NORMAL)
        # cv2.imshow('kernel', kernel)

    # Normalize.
    kernel /= kernel_size

    # Apply the vertical kernel.
    blur_mb = cv2.filter2D(img, -1, kernel)
    # run multiple passes with the same kernel
    # for i in range(10):
    #     blur_mb = cv2.filter2D(blur_mb, -1, kernel)

    # return blur_mb

    # Save the outputs.
    # cv2.imwrite('slope.jpg', blur_mb)

    cv2.imshow('blur', blur_mb)

    while (True):
        k = cv2.waitKey()
        if k == ord("q"):
            break


if __name__ == '__main__':
    # img = cv2.imread('./2023-04-23@17-23-03.334975/' + '0000000052.jpg')
    # img = cv2.convertScaleAbs(img, alpha=1.5, beta=10)
    # angled_motion_blur(img, 0, 50)
    for i in os.listdir('./2023-04-23@17-23-03.334975'):
        img = cv2.imread('./2023-04-23@17-23-03.334975/' + i)
        cv2.convertScaleAbs(img, alpha=1.5, beta=10)
        angled_motion_blur(img, 0, 50)
        cv2.imwrite('./angles/angle_0/'+i,angled_motion_blur(img, 0, 50))
        cv2.imwrite('./angles/angle_15/'+i,angled_motion_blur(img, 15, 50))
        cv2.imwrite('./angles/angle_30/'+i,angled_motion_blur(img, 30, 50))
        cv2.imwrite('./angles/angle_45/'+i,angled_motion_blur(img, 45, 50))
        cv2.imwrite('./angles/angle_-15/'+i,angled_motion_blur(img, -15, 50))
        cv2.imwrite('./angles/angle_-30/'+i,angled_motion_blur(img, -30, 50))
        cv2.imwrite('./angles/angle_-45/'+i,angled_motion_blur(img, -45, 50))
