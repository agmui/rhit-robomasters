from __future__ import print_function
from __future__ import division
import cv2
import numpy as np
import sys
"""
blu lower
[164,98,98]
blu upper
[215,120,136]

white lower
[224,230,231]
white upper
[242,239,241]
"""
RGB_lower = [0, 0, 0]
RGB_upper = [0, 0, 0]


def red_lower(val):
    RGB_lower[0] = val
    slider()


def red_upper(val):
    RGB_upper[0] = val
    slider()


def gre_lower(val):
    RGB_lower[1] = val
    slider()


def gre_upper(val):
    RGB_upper[1] = val
    slider()


def blu_lower(val):
    RGB_lower[2] = val
    slider()


def blu_upper(val):
    RGB_upper[2] = val
    slider()


def slider():
    lower_arr = np.array(RGB_lower)
    upper_arr = np.array(RGB_upper)

    full_mask = cv2.inRange(image, lower_arr, upper_arr)

    final_result = cv2.bitwise_and(result, result, mask=full_mask)
    cv2.imshow(name, final_result)


image = cv2.imread(cv2.samples.findFile("pic/ogCalibration.png"))

if image is None:
    sys.exit("Could not read the image.")

result = image.copy()

"""def adjust_contrast_brightness(img, contrast: float = 1.0, brightness: int = 0):
    # Adjusts contrast and brightness of an uint8 image.
    # contrast:   (0.0,  inf) with 1.0 leaving the contrast as is
    # brightness: [-255, 255] with 0 leaving the brightness as is
    brightness += int(round(255 * (1 - contrast) / 2))
    return cv2.addWeighted(img, contrast, img, 0, brightness)


contrast = 14
brightness = -1395

output = adjust_contrast_brightness(image, contrast, brightness)


B, G, R = cv2.split(output)"""

name: str = "rgb"
cv2.namedWindow(name)
cv2.moveWindow(name, 2500, 150)
cv2.createTrackbar("red lower", name, 0, 255, red_lower)
cv2.createTrackbar("red upper", name, 0, 255, red_upper)
cv2.createTrackbar("green lower", name, 0, 255, gre_lower)
cv2.createTrackbar("green upper", name, 0, 255, gre_upper)
cv2.createTrackbar("blu lower", name, 0, 255, blu_lower)
cv2.createTrackbar("blu upper", name, 0, 255, blu_upper)

red_lower(0)
red_upper(0)
gre_lower(0)
gre_upper(0)
blu_lower(0)
blu_upper(0)

k = cv2.waitKey(0)

while (True):
    k = cv2.waitKey()
    if k == ord("q"):
        break
# if k == ord("s"):
#     cv.imwrite("starry_night.png", img)
