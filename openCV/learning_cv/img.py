from __future__ import print_function
from __future__ import division
import cv2
import numpy as np
import sys


def HSV(image):
    lower_blu = [122, 84, 30]
    upper_blu = [125, 131, 219]
    lower_white = [0, 0, 234]
    upper_white = [132, 18, 242]

    result = image.copy()

    image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)  # converts BGR to HSV

    # ==just blu==
    lower_arr = np.array(lower_blu)
    upper_arr = np.array(upper_blu)

    full_mask = cv2.inRange(image, lower_arr, upper_arr)

    result_blu = cv2.bitwise_and(result, result, mask=full_mask)

    # ==just white==
    lower_arr = np.array(lower_white)
    upper_arr = np.array(upper_white)

    full_mask = cv2.inRange(image, lower_arr, upper_arr)

    result_white = cv2.bitwise_and(result, result, mask=full_mask)

    return result_blu, result_white
    # result = cv2.bitwise_or(result_blu, result_white)
    # return result  # cv2.resize(result, (100, 100))


def RGB(image):
    blu_lower = [164, 98, 98]
    blu_upper = [215, 120, 136]
    white_lower = [224, 230, 231]
    white_upper = [242, 239, 241]

    # ===Blu only===
    result = image.copy()
    lower_arr = np.array(blu_lower)
    upper_arr = np.array(blu_upper)

    full_mask = cv2.inRange(image, lower_arr, upper_arr)

    blu_result = cv2.bitwise_and(result, result, mask=full_mask)

    # ===white only===
    result = image.copy()
    lower_arr = np.array(white_lower)
    upper_arr = np.array(white_upper)

    full_mask = cv2.inRange(image, lower_arr, upper_arr)

    white_result = cv2.bitwise_and(result, result, mask=full_mask)

    return blu_result, white_result


# image = cv2.imread(cv2.samples.findFile("pic/blob.png"))
image = cv2.imread(cv2.samples.findFile("pic/ogCalibration.png"))
# image = cv2.imread(cv2.samples.findFile("pic/2022-04-14_22-36_1.png"))
# image = cv2.imread(cv2.samples.findFile("pic/2022-04-14_22-36_2.png"))

if image is None:
    sys.exit("Could not read the image.")

HSV_result = HSV(image)
RGB_result = RGB(image)
# cv2.imshow("hsv", HSV_result[1])
# cv2.imshow("rgb", RGB_result[1])

final_blu = cv2.bitwise_and(HSV_result[0], RGB_result[0])
final_white = cv2.bitwise_and(HSV_result[1], RGB_result[1])

# final_result = cv2.bitwise_or(final_blu, final_white)
final_result = final_white

name: str = "final output"
cv2.namedWindow(name)
cv2.moveWindow(name, 2500, 150)
# cv2.imshow(name, final_result)
# cv2.imshow("og", image)

"""# =====blob detection=====
im = final_result
# Set up the detector with default parameters.
detector = cv2.SimpleBlobDetector()

# Detect blobs.
keypoints = detector.detect(im)

# Draw detected blobs as red circles.
# cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS ensures the size of the circle corresponds to the size of blob
im_with_keypoints = cv2.drawKeypoints(im, keypoints, np.array([]), (255, 255, 255),
                                      cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

# Show keypoints
cv2.imshow("Keypoints", im_with_keypoints)
"""

# must invert for blob detection
final_result = cv2.bitwise_not(final_result)
final_result = cv2.cvtColor(final_result, cv2.COLOR_BGR2GRAY)  # converts BGR to black and white

params = cv2.SimpleBlobDetector_Params()

params.filterByArea = True
params.minArea = 100

params.filterByConvexity = True
params.minConvexity = 0.2

params.filterByInertia = True
params.minInertiaRatio = 0.01

detector = cv2.SimpleBlobDetector_create(params)
keypoints = detector.detect(final_result)

blank = np.zeros((1, 1))
blobs = cv2.drawKeypoints(image, keypoints, blank, (0, 0, 255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

cv2.imshow(name, blobs)

cv2.waitKey(0)
while True:
    k = cv2.waitKey(0)
    if k == ord("q"):
        cv2.destroyAllWindows()
        break
