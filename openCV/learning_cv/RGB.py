from __future__ import print_function
from __future__ import division
import cv2
import numpy as np
import sys
import os
from definitions import ROOT_DIR

"""
black
lower = [33, 0, 0]
upper = [121, 61, 67]

blue
lower = [239, 200, 0]
upper = [255, 255, 158]

red
lower = [0, 0, 242]
upper = [111, 145, 255]
"""

lower = [0, 0, 242]
upper = [111, 145, 255]
RGB = [lower, upper]


def hue_lower(H):
    lower[0] = H
    slider()


def sat_lower(S):
    lower[1] = S
    slider()


def val_lower(V):
    lower[2] = V
    slider()


def hue_upper(H):
    upper[0] = H
    slider()


def sat_upper(S):
    upper[1] = S
    slider()


def val_upper(V):
    upper[2] = V
    slider()


def slider():
    lower_arr = np.array(lower)
    upper_arr = np.array(upper)

    full_mask = cv2.inRange(image, lower_arr, upper_arr)

    final_result = cv2.bitwise_and(result, result, mask=full_mask)
    cv2.imshow(name, final_result)


# image = cv2.imread(os.path.join(ROOT_DIR, "pic/multiBluPanel.jpg"))  # multi panel
# image = cv2.imread(os.path.join(ROOT_DIR, "pic/2panelBlu.jpg"))
# image = cv2.imread(os.path.join(ROOT_DIR,"pic/IMG_0428.JPG"))
# image = cv2.imread(os.path.join(ROOT_DIR ,"pic/IMG_0429.JPG"))
# image = cv2.resize(image, (image.shape[1]//4, image.shape[0]//4))  # Resize image
image = cv2.imread(os.path.join(ROOT_DIR,"pic/compRed.png"))

if image is None:
    sys.exit("Could not read the image.")

result = image.copy()

name: str = "rgb"
cv2.namedWindow(name)
# cv2.moveWindow(name, 2500, 150)

cv2.createTrackbar("RL", name, 0, 255, hue_lower)
cv2.setTrackbarPos("RL", name, lower[0])

cv2.createTrackbar("GL", name, 0, 255, sat_lower)
cv2.setTrackbarPos("GL", name, lower[1])

cv2.createTrackbar("BL", name, 0, 255, val_lower)
cv2.setTrackbarPos("BL", name, lower[2])

cv2.createTrackbar("RU", name, 0, 255, hue_upper)
cv2.setTrackbarPos("RU", name, upper[0])

cv2.createTrackbar("GU", name, 0, 255, sat_upper)
cv2.setTrackbarPos("GU", name, upper[1])

cv2.createTrackbar("BU", name, 0, 255, val_upper)
cv2.setTrackbarPos("BU", name, upper[2])

# Show some stuff
hue_lower(lower[0])
sat_lower(lower[1])
val_lower(lower[2])
hue_upper(upper[0])
sat_upper(upper[1])
val_upper(upper[2])
# Wait until user press some key
while True:
    k = cv2.waitKey()
    if k == ord("q"):
        break
