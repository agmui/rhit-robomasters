import cv2
import numpy as np


def HSV(image):
    lower_blu = [122, 84, 30]
    upper_blu = [125, 131, 219]
    lower_white = [0, 0, 234]
    upper_white = [132, 18, 242]

    result = image.copy()

    image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)  # converts BGR to HSV

    # ==just blu==
    lower_arr = np.array(lower_blu)
    upper_arr = np.array(upper_blu)

    full_mask = cv2.inRange(image, lower_arr, upper_arr)

    result_blu = cv2.bitwise_and(result, result, mask=full_mask)

    # ==just white==
    lower_arr = np.array(lower_white)
    upper_arr = np.array(upper_white)

    full_mask = cv2.inRange(image, lower_arr, upper_arr)

    result_white = cv2.bitwise_and(result, result, mask=full_mask)

    return result_blu, result_white
    # result = cv2.bitwise_or(result_blu, result_white)
    # return result  # cv2.resize(result, (100, 100))


def RGB(image):
    blu_lower = [164, 98, 98]
    blu_upper = [215, 120, 136]
    white_lower = [224, 230, 231]
    white_upper = [242, 239, 241]

    # ===Blu only===
    result = image.copy()
    lower_arr = np.array(blu_lower)
    upper_arr = np.array(blu_upper)

    full_mask = cv2.inRange(image, lower_arr, upper_arr)

    blu_result = cv2.bitwise_and(result, result, mask=full_mask)

    # ===white only===
    result = image.copy()
    lower_arr = np.array(white_lower)
    upper_arr = np.array(white_upper)

    full_mask = cv2.inRange(image, lower_arr, upper_arr)

    white_result = cv2.bitwise_and(result, result, mask=full_mask)

    return blu_result, white_result


# Create a VideoCapture object
# cv2.VideoCapture(0): Means first camera or webcam.
# cv2.VideoCapture(1):  Means second camera or webcam.
# cv2.VideoCapture("file name.mp4"): Means video file
cap = cv2.VideoCapture(0)

# Check if camera opened successfully
if not cap.isOpened():
    print("Error opening video  file")

# Read until video is completed
while cap.isOpened():

    # Capture frame-by-frame
    ret, frame = cap.read()
    if not ret:
        break

    HSV_result = HSV(frame)
    RGB_result = RGB(frame)

    final_blu = cv2.bitwise_and(HSV_result[0], RGB_result[0])
    final_white = cv2.bitwise_and(HSV_result[1], RGB_result[1])

    final_result = final_white

    name: str = "final output"
    cv2.namedWindow(name)
    cv2.moveWindow(name, 2500, 150)

    # =====blob detection=====

    # must invert for blob detection
    final_result = cv2.bitwise_not(final_result)
    final_result = cv2.cvtColor(final_result, cv2.COLOR_BGR2GRAY)  # converts BGR to black and white

    params = cv2.SimpleBlobDetector_Params()

    params.filterByArea = True
    params.minArea = 100

    params.filterByConvexity = True
    params.minConvexity = 0.2

    params.filterByInertia = True
    params.minInertiaRatio = 0.01

    detector = cv2.SimpleBlobDetector_create(params)
    keypoints = detector.detect(final_result)

    blank = np.zeros((1, 1))
    blobs = cv2.drawKeypoints(frame, keypoints, blank, (0, 0, 255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

    cv2.imshow("og", final_white)
    cv2.imshow(name, blobs)

    if cv2.waitKey(25) & 0xFF == ord('q'):
        break

# When everything done, release
# the video capture object
cap.release()

# Closes all the frames
cv2.destroyAllWindows()
