import filters
import cv2
from definitions import ROOT_DIR
import os

image = cv2.imread(os.path.join(ROOT_DIR, "pic/IMG_0415.JPG"))  # normal square panel
image = cv2.resize(image, (image.shape[1] // 4, image.shape[0] // 4))  # Resize image
x, y, _ = image.shape
rect = [[0, 0], [x, 0], [0, y], [x, y]]
print(rect)

cv2.rectangle(image, rect[0], rect[3], 2)
filters.perspective_transform(image, rect, 'smolRed', True)
cv2.imshow("before", image)

while True:
    k = cv2.waitKey(1)
    if k == ord("q"):
        cv2.destroyAllWindows()
        break
