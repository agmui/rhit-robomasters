from __future__ import print_function
from __future__ import division
import cv2
import numpy as np
import sys

"""
just blu
lower: [122, 84, 30]
upper: [125, 131, 219]

just white
lower: [0,0,234]
upper: [132,18,242]
"""
lower_blu = [122, 84, 30]
upper_blu = [125, 131, 219]
HSV_blu = [lower_blu, upper_blu]

lower_white = [0, 0, 234]
upper_white = [132, 18, 242]
HSV_white = [lower_white, upper_white]

image = cv2.imread(cv2.samples.findFile("pic/ogCalibration.png"))

if image is None:
    sys.exit("Could not read the image.")

result = image.copy()

image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

# ==just blu==
lower_arr = np.array(lower_blu)
upper_arr = np.array(upper_blu)

full_mask = cv2.inRange(image, lower_arr, upper_arr)

result_blu = cv2.bitwise_and(result, result, mask=full_mask)

# ==just white==
lower_arr = np.array(lower_white)
upper_arr = np.array(upper_white)

full_mask = cv2.inRange(image, lower_arr, upper_arr)

result_white = cv2.bitwise_and(result, result, mask=full_mask)

name: str = "hsv"
cv2.namedWindow(name)
cv2.moveWindow(name, 2500, 150)
# cv2.imshow("blu", result_blu)
# cv2.imshow("white", result_white)
final_result = cv2.bitwise_or(result_blu,result_white)
cv2.imshow(name, final_result)

# Wait until user press some key
while (True):
    k = cv2.waitKey()
    if k == ord("q"):
        break
