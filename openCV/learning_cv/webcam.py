# import the opencv library
import cv2
import crappyV2_CV

# define a video capture object
vid = cv2.VideoCapture(0)

while (True):
    ret, frame = vid.read()

    cv2.imshow('frame', frame)
    # crappyV2_CV.objectDetection(frame, 'red')

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# After the loop release the cap object
vid.release()
# Destroy all the windows
cv2.destroyAllWindows()
