import os
import cv2
import filters
from definitions import ROOT_DIR

# TODO make massive comparison program
# image = cv2.imread(os.path.join(ROOT_DIR, "pic/IMG_0415.JPG"))  # normal square panel
# rect = [[288, 317], [435, 325], [255, 453], [400, 480]]
# rect = [[0,0], [0,100], [100,0], [100,100]]
rect = [[288-50, 317-50], [435+50, 325-50], [255-50, 453+50], [400+50, 480+50]]

# image = cv2.imread(os.path.join(ROOT_DIR, "pic/edited2panelBlu.jpg"))
# image = cv2.imread(os.path.join(ROOT_DIR, "pic/nums.png"))
image = cv2.imread(os.path.join(ROOT_DIR, "pic/finalSmolBlu.jpg"))
# image = cv2.resize(image, (int(image.shape[1] // 2), int(image.shape[0] // 2)))  # Resize image

# .08471628278493881, 039498504251241684 F
# .10388404875993729
# 60768280

drawImg = image.copy()

# cv2.rectangle(drawImg, rect[0], rect[3], (0, 255, 0), 2)
# cv2.imshow("og", drawImg)
# image = filters.perspective_transform(image, rect, 'smolRed', False)
# image = cv2.resize(image, (image.shape[1] * 2, image.shape[0] * 2))  # Resize image

th = filters.adaptiveGaussianThresh(image, False)
filters.convolutionMatching(th, 'smolRed', True, image)

while True:
    k = cv2.waitKey(1)
    if k == ord("q"):
        cv2.destroyAllWindows()
        break
