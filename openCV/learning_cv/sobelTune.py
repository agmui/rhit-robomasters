import cv2
import sys

params = [3, 3, 0]


def kernelWidth(H):
    params[0] = 2*H-1
    filterFunc()


def kernelHeight(S):
    params[1] = 2*S-1
    filterFunc()


def deviation(S):
    params[2] = S
    filterFunc()


def filterFunc():
    # ==sobel ED==
    # Convert to graycsale
    img_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # Blur the image for better edge detection
    img_blur = cv2.GaussianBlur(img_gray, (params[0], params[1]), params[2])
    # Sobel Edge Detection
    sobelx = cv2.Sobel(src=img_blur, ddepth=cv2.CV_64F, dx=1, dy=0, ksize=5)  # Sobel Edge Detection on the X axis
    sobely = cv2.Sobel(src=img_blur, ddepth=cv2.CV_64F, dx=0, dy=1, ksize=5)  # Sobel Edge Detection on the Y axis
    sobelxy = cv2.Sobel(src=img_blur, ddepth=cv2.CV_64F, dx=1, dy=1, ksize=5)  # Combined X and Y Sobel Edge Detection
    # Display Sobel Edge Detection Images
    cv2.imshow('img', img_blur)
    cv2.imshow(name+'X', sobelx)
    cv2.imshow(name+'Y', sobely)
    cv2.imshow(name, sobelxy)
    # ==


image = cv2.imread(cv2.samples.findFile("../pic/IMG_0434.JPG"))
image = cv2.resize(image, (image.shape[1] // 3, image.shape[0] // 3))  # Resize image

if image is None:
    sys.exit("Could not read the image.")

result = image.copy()
name: str = "sobel ED"
cv2.namedWindow(name)
cv2.moveWindow(name, 2500, 150)

cv2.createTrackbar("blur width", name, 1, 11, kernelWidth)
cv2.setTrackbarPos("blur width", name, params[0])

cv2.createTrackbar("blur height", name, 1, 11, kernelHeight)
cv2.setTrackbarPos("blur height", name, params[1])

cv2.createTrackbar("deviation", name, 0, 21, deviation)
cv2.setTrackbarPos("deviation", name, params[2])

kernelWidth(params[0])
kernelHeight(params[1])
deviation(params[2])


while (True):
    k = cv2.waitKey()
    if k == ord("q"):
        break
