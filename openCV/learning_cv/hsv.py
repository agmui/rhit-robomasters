from __future__ import print_function
from __future__ import division
import cv2
import numpy as np
import sys
import os
from definitions import ROOT_DIR

"""
black
lower 110,046,37
upper 120, 187, 202

blue
lower = [89, 87, 0]
upper = [94, 255, 255]
"""

lower = [0, 131, 244]
upper = [180, 255, 255]
HSV = [lower, upper]


def hue_lower(H):
    lower[0] = H
    slider()


def sat_lower(S):
    lower[1] = S
    slider()


def val_lower(V):
    lower[2] = V
    slider()


def hue_upper(H):
    upper[0] = H
    slider()


def sat_upper(S):
    upper[1] = S
    slider()


def val_upper(V):
    upper[2] = V
    slider()


def slider():
    lower_arr = np.array(lower)
    upper_arr = np.array(upper)

    full_mask = cv2.inRange(image, lower_arr, upper_arr)

    final_result = cv2.bitwise_and(result, result, mask=full_mask)
    cv2.imshow(name, final_result)


image = cv2.imread(os.path.join(ROOT_DIR, "pic/multiBluPanel.jpg"))  # multi panel
image = cv2.resize(image, (image.shape[1]//3, image.shape[0]//3))  # Resize image

if image is None:
    sys.exit("Could not read the image.")

# im_show("Original", image, 0)

result = image.copy()

image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

# ==RED==
# # lower boundary RED color range values; Hue (0 - 10)
# lower1 = np.array([0, 100, 20])
# upper1 = np.array([10, 255, 255])
#
# # upper boundary RED color range values; Hue (160 - 180)
# lower2 = np.array([160, 100, 20])
# upper2 = np.array([179, 255, 255])
#
# lower_mask = cv2.inRange(image, lower1, upper1)
# upper_mask = cv2.inRange(image, lower2, upper2)
#
# full_mask = lower_mask + upper_mask
# ====

# ==BLUE==
# # upper boundary BLUE color range values; Hue (160 - 180)
# lower = np.array([90, 100, 20])
# upper = np.array([120, 255, 255])
#
# full_mask = cv2.inRange(image, lower, upper)
# =====


name: str = "hsv"
cv2.namedWindow(name)
# cv2.moveWindow(name, 2500, 150)

cv2.createTrackbar("HL", name, 0, 180, hue_lower)
cv2.setTrackbarPos("HL", name, lower[0])

cv2.createTrackbar("SL", name, 0, 255, sat_lower)
cv2.setTrackbarPos("SL", name, lower[1])

cv2.createTrackbar("VL", name, 0, 255, val_lower)
cv2.setTrackbarPos("VL", name, lower[2])

cv2.createTrackbar("HU", name, 0, 180, hue_upper)
cv2.setTrackbarPos("HU", name, upper[0])

cv2.createTrackbar("SU", name, 0, 255, sat_upper)
cv2.setTrackbarPos("SU", name, upper[1])

cv2.createTrackbar("VU", name, 0, 255, val_upper)
cv2.setTrackbarPos("VU", name, upper[2])

# Show some stuff
hue_lower(lower[0])
sat_lower(lower[1])
val_lower(lower[2])
hue_upper(upper[0])
sat_upper(upper[1])
val_upper(upper[2])
# Wait until user press some key
while (True):
    k = cv2.waitKey()
    if k == ord("q"):
        break

# lower_arr = np.array(lower)
# upper_arr = np.array(upper)
#
# full_mask = cv2.inRange(image, lower_arr, upper_arr)
#
# result = cv2.bitwise_and(result, result, mask=full_mask)

# im_show('mask', full_mask, 1)
# im_show('result', result, 2)
