from definitions import ROOT_DIR

# import rospy
# from sensor_msgs.msg import Image as msg_Image
# from sensor_msgs.msg import CameraInfo
# from cv_bridge import CvBridge, CvBridgeError
import pyrealsense2 as rs

import pyrealsense2.pyrealsense2 as rs
import numpy as np
import cv2
import crappyV2_CV
import gimbal
# import serial
import filters

exposure_val = 25 #10
name: str = "RealSense"
cv2.namedWindow(name)


def change_exposure(val):
    global exposure_val
    exposure_val = val


# ========init code for intel RealSense=======
# def initCam():
# Configure depth and color streams
pipeline = rs.pipeline()
config = rs.config()

# ==change exposure==
# Get device product line for setting a supporting resolution
pipeline_wrapper = rs.pipeline_wrapper(pipeline)
pipeline_profile = config.resolve(pipeline_wrapper)
device = pipeline_profile.get_device()
s = device.query_sensors()[1]
cv2.createTrackbar("exposure", name, 0, 255, change_exposure)
cv2.setTrackbarPos("exposure", name, exposure_val)
filters.make_slid(name)
device_product_line = str(device.get_info(rs.camera_info.product_line))

config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)

# Start streaming
pipe_profile = pipeline.start(config)
# =========================================
# ==start serial==
# ser = serial.Serial()
# ser.baudrate = 115200
# ser.port = '/dev/ttyTHS0'
try:
    # ser.open()
    print("Port has been opened")
except Exception:
    print("error open serial port: ")
    exit()

try:
    while True:
        # TODO record, and log to file

        s.set_option(rs.option.exposure, exposure_val)
        # Wait for a coherent pair of frames: depth and color
        frames = pipeline.wait_for_frames()
        color_frame = frames.get_color_frame()
        depth_frame = frames.get_depth_frame()

        # Convert images to numpy arrays
        color_image = np.asanyarray(color_frame.get_data())

        # ==opencv filtering==
        panelCords, img = crappyV2_CV.objectDetection(color_image, 'red', True)

        # if len(panelCords) == 0:
        #     continue

        # ==convert to motor codes==
        if len(panelCords) != 0:
            theta, phi = gimbal.convert(panelCords, color_frame, depth_frame, pipe_profile)
        # ==send to MCB==
        # ser.write(f"{theta}|{phi}\n\0".encode())
        # print(f"sent:{theta}|{phi}")


        k = cv2.waitKey(1)
        if k == ord("q"):
            cv2.destroyAllWindows()
            break


finally:
    # Stop streaming
    pipeline.stop()
